import 'dart:io';

import 'package:shelf/shelf.dart';

Response welcome(Request req) {
  return Response.ok('hello _!\n');
}

Future<Response> world(Request request) async {
  final bytes = await File('assets/one').readAsBytes();
  return Response.ok(
    bytes,
    headers: {
      'Content-Type': 'application/octet-stream',
      'Content-Length': bytes.length.toString(),
    },
  );
}

Future<Response> answer(Request request) async {
  final bytes = await File('assets/bin').readAsBytes();
  return Response.ok(
    bytes,
    headers: {
      'Content-Type': 'application/octet-stream',
      'Content-Length': bytes.length.toString(),
    },
  );
}
