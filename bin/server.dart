import 'dart:io';

import 'package:shelf/shelf.dart';
import 'package:shelf/shelf_io.dart';
import 'package:shelf_router/shelf_router.dart';

import 'routes.dart';

// Configure routes.
final _router = Router()
  ..get('/', welcome)
  ..get('/world', world)
  ..get('/straight/right/right/right/left/straight/left/right', answer);

void main(List<String> args) async {
  final ip = InternetAddress.tryParse('0.0.0.0')!;
  final handler = Pipeline().addMiddleware(logRequests()).addHandler(_router);
  final port = int.parse(Platform.environment['PORT'] ?? '42069');
  serve(handler, ip, port);
}
